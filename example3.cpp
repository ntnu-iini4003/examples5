#include <iostream>
#include <memory>
#include <string>
#include <vector>

using namespace std;

class Material {
public:
  string name;
  double price; // per unit

  Material(const string &name_, double price_) : name(name_), price(price_) {}
  virtual ~Material() {}

  virtual void print() const {
    cout << "Navn: " << name << endl
         << "Pris: " << price << endl;
  };
};

class Covering : public Material {
public:
  double width; // i meter

  // Prisen er per meter belegg (i lengden)
  Covering(const string &name, double price, double width) : Material(name, price), width(width) {}

  void print() const override {
    Material::print();
    cout << "For belegg" << endl
         << "Bredde: " << width << endl;
  }
};

class Tapet : public Material {
public:
  double length; // i meter per rull
  double width;  // i meter per rull

  // Prisen er per rull
  Tapet(const string &name, double price, double length_, double width_)
      : Material(name, price), length(length_), width(width_) {}

  void print() const override {
    Material::print();
    cout << "For tapet" << endl
         << "Bredde av rull: " << width << endl
         << "Lengde av rull: " << length << endl;
  }
};

class Paint : public Material {
public:
  int coatings;         // Antall strøk
  double liters_per_m2; // Liter per kvadratmeter

  // Prisen er per liter
  Paint(const string &name, double price, int coatings_, double liters_per_m2_)
      : Material(name, price), coatings(coatings_), liters_per_m2(liters_per_m2_) {}

  void print() const override {
    Material::print();
    cout << "For maling" << endl
         << "Antall strøk: " << coatings << endl
         << "Ant kvm./l: " << liters_per_m2 << endl;
  }
};

int main() {
  vector<unique_ptr<Material>> materials;
  materials.emplace_back(new Covering("Super Duper Dux", 433.50, 4));
  materials.emplace_back(new Tapet("Soldogg", 200, 12, 0.6));
  materials.emplace_back(new Paint("Extra", 125, 2, 12.0));

  for (auto &material : materials) {
    material->print();
    cout << endl;
  }
}
