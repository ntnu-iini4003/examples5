#include <iostream>
#include <memory>
#include <string>
#include <vector>

using namespace std;

class Surface {
public:
  string name;
  double length;
  double width;

  Surface(const string name_, double length_, double width_) : name(name_), length(length_), width(width_) {}

  double area() const {
    return width * length;
  }
  double circumference() const {
    return 2 * (length + width);
  }
};

class Material {
public:
  string name;
  double price; // per unit

  Material(const string &name_, double price_) : name(name_), price(price_) {}
  virtual ~Material() {}

  virtual void print() const {
    cout << "Navn: " << name << endl
         << "Pris: " << price << endl;
  };

  double price_total(const Surface &surface) const {
    return units_needed(surface) * price;
  }

  virtual double units_needed(const Surface &surface) const = 0;
};

const double limit = 0.02; // ved beregning av materialbehov

class Covering : public Material {
public:
  double width; // i meter

  // Prisen er per meter belegg (i lengden)
  Covering(const string &name, double price, double width_) : Material(name, price), width(width_) {}

  void print() const override {
    Material::print();
    cout << "For belegg" << endl
         << "Bredde: " << width << endl;
  }

  double units_needed(const Surface &surface) const override {
    int lengths = surface.length / width;
    double remainder = surface.length - lengths * width;
    if (remainder >= limit)
      ++lengths;
    return lengths * surface.width;
  }
};

class Wallpaper : public Material {
public:
  double length; // i meter per rull
  double width;  // i meter per rull

  // Prisen er per rull
  Wallpaper(const string &name, double price, double length_, double width_)
      : Material(name, price), length(length_), width(width_) {}

  void print() const override {
    Material::print();
    cout << "For tapet" << endl
         << "Bredde av rull: " << width << endl
         << "Lengde av rull: " << length << endl;
  }

  double units_needed(const Surface &surface) const override {
    // finner antall hoyder vi trenger
    int lengths = surface.length / width;
    double remainder = surface.length - lengths * width;
    if (remainder >= limit)
      ++lengths;

    // finner antall ruller som trengs
    int rolls;
    int roll_lengths = length / surface.width;
    if (roll_lengths > 0) {
      rolls = lengths / roll_lengths;
      remainder = lengths - rolls * roll_lengths;
      if (remainder >= limit)
        rolls++;
    }
    else { // rullen er for kort til å rekke en hoyde (sjelden!)
      double total = lengths * surface.width;
      rolls = total / length;
      if ((total - rolls * length) >= limit)
        rolls++;
    }
    return rolls;
  }
};

class Paint : public Material {
public:
  int coatings;         // Antall strøk
  double liters_per_m2; // Liter per kvadratmeter

  // Prisen er per liter
  Paint(const string &name, double price, int coatings_, double liters_per_m2_)
      : Material(name, price), coatings(coatings_), liters_per_m2(liters_per_m2_) {}

  void print() const override {
    Material::print();
    cout << "For maling" << endl
         << "Antall strøk: " << coatings << endl
         << "Ant kvm./l: " << liters_per_m2 << endl;
  }

  double units_needed(const Surface &surface) const override {
    double liters = surface.area() * coatings / liters_per_m2;
    int liters_int = liters;
    double remainder = liters - liters_int;
    if (remainder >= 0.5 + limit)
      return liters_int + 1.0;
    else if (remainder >= limit)
      return liters_int + 0.5;
    else
      return liters_int;
  }
};

int main() {
  vector<std::unique_ptr<Material>> materials;
  materials.emplace_back(new Covering("Ull-Gull", 450, 5));
  materials.emplace_back(new Wallpaper("Brokade", 500, 12, 0.6));
  materials.emplace_back(new Paint("Heimdal XX-Supre", 200, 2, 10));

  Surface surface("vegg", 4, 5);

  for (auto &material : materials) {
    cout << material->name << ": "
         << " Behov: " << material->units_needed(surface)
         << " enheter, pris: " << material->price_total(surface) << endl;
  }
}
